from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from device.models import Device
from device.serializers import DeviceSerializer

# Create your views here.
class DeviceViewSet(ModelViewSet):
    queryset = Device.objects.all() # pylint: disable=E1101
    lookup_field = 'slug'
    serializer_class = DeviceSerializer
    permission_classes = [AllowAny,]
