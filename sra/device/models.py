from django.db import models
from room.models import Room
from django.utils.text import slugify

# Create your models here.
class Device(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE,
                             verbose_name="Urządzenie")
    name = models.CharField(max_length=127, verbose_name="Nazwa")
    description = models.TextField(verbose_name="Opis")
    slug = models.SlugField(max_length=50, unique=True, blank=True,
                            verbose_name="Slug")

    class Meta:
        verbose_name = "Urządzenie"
        verbose_name_plural = "Urządzenia"

    def _get_unique_slug(self):
        slug = slugify(self.name)
        unique_slug = slug
        num = 1
        while Device.objects.filter(slug=unique_slug).exists(): # pylint: disable=E1101
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)
