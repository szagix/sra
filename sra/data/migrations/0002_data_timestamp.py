# Generated by Django 2.1.5 on 2019-02-09 00:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='data',
            name='timestamp',
            field=models.DateTimeField(auto_now=True, verbose_name='Czas utworzenia'),
        ),
    ]
