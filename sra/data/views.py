from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from data.models import Data
from data.serializers import DataSerializer

# Create your views here.
class DataViewSet(ModelViewSet):
    queryset = Data.objects.all() # pylint: disable=E1101
    serializer_class = DataSerializer
    permission_classes = [AllowAny,]

    def get_queryset(self):
        return Data.objects.all().order_by('-timestamp') # pylint: disable=E1101
