from django.db import models
from device.models import Device

# Create your models here.
class Data(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE,
                               to_field='slug', verbose_name="Dane")
    timestamp = models.DateTimeField(auto_now=True, verbose_name="Czas utworzenia")
    temperature = models.CharField(max_length=127, verbose_name="Temperatura")
    humidity = models.CharField(max_length=127, verbose_name="Wilgotność")
    pressure = models.CharField(max_length=127, verbose_name="Ciśnienie")

    class Meta:
        verbose_name = "Dane"
        verbose_name_plural = "Dane"
