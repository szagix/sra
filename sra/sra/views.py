from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from sra.serializers import UserSerializer

# Create your views here.
class UserViewSet(ModelViewSet):
    queryset = User.objects.all() # pylint: disable=E1101
    lookup_field = 'username'
    serializer_class = UserSerializer
    permission_classes = [AllowAny,]
