"""sra URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, include
from rest_framework.routers import DefaultRouter

from house.views import HouseViewSet
from room.views import RoomViewSet
from device.views import DeviceViewSet
from data.views import DataViewSet
from sra.views import UserViewSet

# Default router for API.
ROUTER = DefaultRouter()
ROUTER.register(r'house', HouseViewSet)
ROUTER.register(r'room', RoomViewSet)
ROUTER.register(r'device', DeviceViewSet)
ROUTER.register(r'data', DataViewSet)
ROUTER.register(r'user', UserViewSet)


urlpatterns = [
    re_path(r'admin/', admin.site.urls),
    re_path(r'api/(?P<version>(v1))/', include(ROUTER.urls)),
]
