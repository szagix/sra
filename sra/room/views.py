from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from room.models import Room
from room.serializers import RoomSerializer

# Create your views here.
class RoomViewSet(ModelViewSet):
    queryset = Room.objects.all() # pylint: disable=E1101
    serializer_class = RoomSerializer
    permission_classes = [AllowAny,]
