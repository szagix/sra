from django.db import models
from house.models import House

class Room(models.Model):
    house = models.ForeignKey(House, on_delete=models.CASCADE,
                              verbose_name="Dom")
    name = models.CharField(max_length=127, verbose_name="Nazwa")

    class Meta:
        verbose_name = "Pomieszczenie"
        verbose_name_plural = "Pomieszczenia"
