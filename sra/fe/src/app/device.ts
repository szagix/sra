import { Room } from './room';

export class Device {
  room: Room;
  name: string;
  description: string;
  slug: string;
}
