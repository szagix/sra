import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './auth/auth.component';
import { DeviceComponent } from './device/device.component';
import { DeviceDetailComponent } from './device-detail/device-detail.component';
import { HouseComponent } from './house/house.component';
import { RoomComponent } from './room/room.component';

const routes: Routes = [
  { path: '', component: AuthComponent },
  { path: 'house', component: HouseComponent },
  { path: 'house/:id', component: RoomComponent },
  { path: 'room/:id', component: DeviceComponent },
  { path: 'device/:id', component: DeviceDetailComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
