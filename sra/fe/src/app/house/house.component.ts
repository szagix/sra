import { Component, OnInit } from '@angular/core';
import { HouseService } from '../house.service';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html',
  styleUrls: ['./house.component.css']
})
export class HouseComponent implements OnInit {
  public house: {user: number, name: string, location: string};

  constructor(private houseService: HouseService) {
    this.house = {
      user: 1,
      name: '',
      location: ''
    };
  }

  ngOnInit() {
    this.houseService.getHouses();
  }

  add() {
    this.houseService.add(this.house);
  }

}
