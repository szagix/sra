import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Data } from './data';
import { DeviceService } from './device.service';

import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public data: Data[];
  public latest: Data;

  private dataApi: string = 'api/v1/data';
  private httpOptions: { headers: HttpHeaders };

  constructor(private http: HttpClient, private deviceService: DeviceService) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  getData() {
    this.http.get<Data[]>(this.dataApi, this.httpOptions).subscribe(
      (entries: Data[]) => this.data = entries
    );
  }

  getLatest() {
    this.http.get(this.dataApi, this.httpOptions).subscribe(
      objects => this.latest = objects[0]
    );
  }
}
