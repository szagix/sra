import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Room } from './room';
import { HouseService } from './house.service';

import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoomService {
  public rooms: Room[];

  private roomApi: string = 'api/v1/room';
  private httpOptions: { headers: HttpHeaders };

  constructor(private http: HttpClient, private houseService: HouseService) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  getRooms() {
    this.http.get<Room[]>(this.roomApi, this.httpOptions).subscribe(
      (data: Room[]) => this.rooms = data
    );
  }

  add(room: any) {
    this.http.post(`${this.roomApi}/`, JSON.stringify(room), this.httpOptions).subscribe(
      response => console.log(response)
    );
  }
}
