import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { House } from './house';
import { UserService } from './user.service';

import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HouseService {
  public houses: House[];

  private houseApi: string = 'api/v1/house';
  private httpOptions: { headers: HttpHeaders };

  constructor(private http: HttpClient, private userService: UserService) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  getHouses() {
    this.http.get<House[]>(this.houseApi, this.httpOptions).subscribe(
      (data: House[]) => {
        this.houses = data;
      },
      err => console.error(err),
      () => console.log(this.houses)
    );
  }

  add(house: any) {
    this.http.post(`${this.houseApi}/`, JSON.stringify(house), this.httpOptions).subscribe(
      response => console.log(response)
    );
  }
}
