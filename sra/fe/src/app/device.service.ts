import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Device } from './device';
import { RoomService } from './room.service';

import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  public devices: Device[];

  private deviceApi: string = 'api/v1/device';
  private httpOptions: { headers: HttpHeaders };

  constructor(private http: HttpClient, private roomService: RoomService) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  getDevices() {
    this.http.get<Device[]>(this.deviceApi, this.httpOptions).subscribe(
      (data: Device[]) => this.devices = data
    );
  }

  add(device: any) {
    this.http.post(`${this.deviceApi}/`, JSON.stringify(device), this.httpOptions).subscribe(
      response => console.log(response)
    );
  }
}
