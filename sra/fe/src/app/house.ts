import { User } from './user';

export class House {
  id: number;
  user: User;
  name: string;
  location: string;
}
