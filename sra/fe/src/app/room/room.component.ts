import { Component, OnInit } from '@angular/core';

import { RoomService } from '../room.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {
  public room: {house: number, name: string};

  constructor(private roomService: RoomService) {
    this.room = {
      house: 1,
      name: ''
    };
  }

  ngOnInit() {
    this.roomService.getRooms();
  }

  add() {
    this.roomService.add(this.room);
  }
}
