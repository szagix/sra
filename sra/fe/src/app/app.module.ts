import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { AuthComponent } from './auth/auth.component';
import { HouseComponent } from './house/house.component';
import { RoomComponent } from './room/room.component';
import { DeviceComponent } from './device/device.component';
import { DataComponent } from './data/data.component';
import { HistoryComponent } from './history/history.component';
import { DeviceDetailComponent } from './device-detail/device-detail.component';

import { UserService } from './user.service';
import { RoomService } from './room.service';
import { HouseService } from './house.service';
import { DeviceService } from './device.service';
import { DataService } from './data.service';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AuthComponent,
    HouseComponent,
    RoomComponent,
    DeviceComponent,
    DataComponent,
    HistoryComponent,
    DeviceDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [
    UserService,
    RoomService,
    HouseService,
    DeviceService,
    DataService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
