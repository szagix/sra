import { House } from './house';

export class Room {
  house: House;
  name: string;
}
