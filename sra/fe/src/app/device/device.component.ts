import { Component, OnInit } from '@angular/core';

import { DeviceService } from '../device.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {
  public device: {room: number, name: string, description: string};

  constructor(private deviceService: DeviceService) {
    this.device = {
      room: 1,
      name: '',
      description: ''
    }
  }

  ngOnInit() {
    this.deviceService.getDevices();
  }

  add() {
    this.deviceService.add(this.device);
  }
}
