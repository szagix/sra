import { Device } from './device';

export class Data {
  timestamp: Date;
  device: Device;
  temperature: number;
  humidity: number;
  pressure: number;
}
