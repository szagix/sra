export class User {
  pk: number;
  lastLogin: Date;
  isSuperUser: boolean;
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  isStaff: boolean;
  dateJoined: Date;
}
