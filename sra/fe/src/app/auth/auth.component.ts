import { Component, OnInit } from '@angular/core';

import { UserService } from '../user.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  public user: any;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user = {
      username: '',
      password: ''
    }
  }

  login() {
    this.userService.login(this.user.username, this.user.password);
  }
}
