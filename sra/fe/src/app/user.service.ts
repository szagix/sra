import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public user: User;

  private userApi: string = 'api/v1/user';
  private httpOptions: { headers: HttpHeaders };

  constructor(private http: HttpClient) {
    this.httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    };
  }

  getUser(username: string): Observable<User> {
    const url = `${this.userApi}/${username}`;
    return this.http.get<User>(url, this.httpOptions);
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.userApi, this.httpOptions);
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem('user') != null) {
      if (this.user == null)
        this.user = JSON.parse(localStorage.getItem('user'));
      return true;
    }
    return false;
  }

  login(username: string, password: string) {
    this.getUser(username).subscribe(
      data => {
        this.user = data;
        localStorage.setItem('user', JSON.stringify(this.user))
      },
      err => console.error(err),
      () => console.log(`Logged in successfully as ${this.user.username}.`)
    );
  }

  logout() {
    this.user = null;
    localStorage.removeItem('user');
    console.log('User logged out successfully.');
  }
}
