from django.shortcuts import render

from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny

from house.models import House
from house.serializers import HouseSerializer

# Create your views here.
class HouseViewSet(ModelViewSet):
    queryset = House.objects.all() # pylint: disable=E1101
    serializer_class = HouseSerializer
    permission_classes = [AllowAny,]
