from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class House(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,
                             verbose_name="Właściciel")
    name = models.CharField(max_length=127, verbose_name="Nazwa")
    location = models.CharField(max_length=255, verbose_name="Położenie")

    class Meta:
        verbose_name = "Dom"
        verbose_name_plural = "Domy"
