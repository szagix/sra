/**
   BasicHTTPClient.ino

    Created on: 24.05.2015

*/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include "DHT.h"

#include <WiFiClient.h>

#define DHTPIN D5
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

ESP8266WiFiMulti WiFiMulti;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("AndroidAP", "pbuu4715");
}

void loop() {
  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {

    WiFiClient client;

    HTTPClient http;

    Serial.print("[HTTP] begin...\n");
    if (http.begin(client, "http://80.211.249.230/api/v1/data/")) {  // HTTP

      Serial.print("[HTTP] POST...\n");
      // send HTTP POST header
      http.addHeader(String("Content-Type"), String("application/json"));

      for (int i=0; i<http.headers(); ++i) {
        Serial.print("w petli for");
        Serial.print(http.header(i));
      }

      float h = dht.readHumidity();
      float t = dht.readTemperature();

      float hic = dht.computeHeatIndex(t, h, false);

      Serial.print(t, 2);
      Serial.print(h, 2);
      Serial.print(hic, 2);

      String json_data = "{\"device\":\"czujnik-1\",\"temperature\":\"";
      json_data += String(t, 2) + "\",\"humidity\":\"" + String(h, 2) + "\",\"pressure\":\"" + String(hic, 2) + "\"}";

      Serial.print("JSON Data\n");
      Serial.print(json_data);
      Serial.print("\n==END==\n");
      
      int httpCode = http.POST(json_data);

      if (httpCode > 0) {
        Serial.printf("[HTTP] POST code: %d\n", httpCode);

        //if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        //}
      } else {
        Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }
  }

  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(10000);
}
